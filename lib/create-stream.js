/* eslint brace-style: ["error", "stroustrup", { "allowSingleLine": true }] */

const RECEIVE_TIME = 'receiveTime'
const UPDATE_TIME = 'updateTime'
const CREATE_TIME = 'createTime'

// creates a stream of potential recordIds
module.exports = function CreateStream (ssb, spec) {
  if (ssb.db) return CreateDB2Stream(ssb, spec)
  else return CreateDB1Stream(ssb, spec)
}

function CreateDB1Stream (ssb, spec) {
  return function createDB1Stream (opts) {
    const orderBy = opts.orderBy || opts.sortBy
    let $filter

    if (orderBy === RECEIVE_TIME || orderBy === undefined) {
      $filter = {
        timestamp: { $gt: 0 }, // stimulates orderBy received time
        value: {
          content: {
            type: spec.type,
            tangles: {
              [spec.tangle]: { root: null } // root messages
            }
          }
        }
      }
    }
    else if (orderBy === CREATE_TIME) {
      $filter = {
        value: {
          timestamp: { $gt: 0 }, // stimulates orderBy (asserted) create time
          content: {
            type: spec.type,
            tangles: {
              [spec.tangle]: { root: null } // root messages
            }
          }
        }
      }
    }
    else if (orderBy === UPDATE_TIME) {
      $filter = {
        timestamp: { $gt: 0 }, // stimulates orderBy received time
        value: {
          content: {
            type: spec.type,
            tangles: {
              [spec.tangle]: { $is: 'object' } // either root/update!
            }
          }
        }
      }
    }
    else throw new Error(`unknown opts.orderBy ${orderBy}`)
    // these are a common ssb-query hack to stimulate the the right time-ordering index
    // tya = "type, asserted timestamp"
    // tyr = "type, received timestamp"

    return ssb.query.read({
      query: [{ $filter }],
      // ...opts,
      reverse: opts.descending !== false
    })
  }
}

function CreateDB2Stream (ssb, spec) {
  const tangleRoot = require('./build-tangle-root-operator')(spec.tangle)
  const {
    where, and,
    slowPredicate, type,
    descending, sortByArrival,
    toPullStream
  } = ssb.db.operators

  return function createDB2Stream (opts) {
    return ssb.db.query(
      where(
        and(
          type(spec.type),
          (opts.orderBy === UPDATE_TIME)
            ? slowPredicate(`value.content.tangles.${spec.tangle}.root`, isAny)
            : tangleRoot(null)
          // NOTE: this could be better written, can look at it later
        )
      ),
      opts.descending === false ? null : descending(),
      (opts.orderBy || opts.sortBy) === CREATE_TIME ? null : sortByArrival(),
      toPullStream()
    )
  }
}

function isAny (root) {
  return (
    root === null ||
    typeof root === 'string'
  )
}
