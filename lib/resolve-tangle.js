const Reduce = require('@tangle/reduce')

const GetRoot = require('./get-root')
const GetUpdates = require('./get-updates')
const { MsgsToNodes } = require('./util')

module.exports = function ResolveTangle (ssb, crut) {
  const { spec, strategy } = crut

  const getRoot = GetRoot(ssb, crut)
  const getUpdates = GetUpdates(ssb, crut)

  const msgsToNodes = MsgsToNodes(spec)

  return function resolveTangle (id, cb) {
    // First, the root message and it's update messages are fetched from the ssb
    getRoot(id, (err, root) => {
      if (err) return cb(err)

      getUpdates(id, (err, updates) => {
        if (err) return cb(err)
        // First, the root message and it's update messages are fetched from the ssb
        const msgs = [root, ...updates]

        const reduce = new Reduce(strategy, {
          nodes: msgsToNodes(msgs),
          isValidNextStep: (context, node) => {
            context.root = root
            return spec.isValidNextStep(context, node, ssb)
          }
        })

        const tips = Object.entries(reduce.state)
          .map(([key, T]) => ({ key, T }))
          .sort(TimestampComparer(msgs))

        cb(null, { root, tips, graph: reduce.graph })
      })
    })
  }
}

function TimestampComparer (msgs) {
  const timestamps = msgs.reduce((timestamps, m) => {
    timestamps[m.key] = m.value.timestamp
    return timestamps
  }, {})

  let result
  return (A, B) => {
    result = timestamps[B.key] - timestamps[A.key]
    if (result !== 0) return result

    return A.key < B.key ? -1 : 1
  }
}
