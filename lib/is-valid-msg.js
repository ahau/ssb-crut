const Validator = require('is-my-ssb-valid')
const definitions = require('ssb-schema-definitions')()

function buildIsRoot ({ spec, strategy }) {
  const { typePattern, tangle, staticProps, nextStepData, hooks } = spec
  const { properties } = strategy.schema

  const schema = {
    type: 'object',
    required: ['type', 'tangles'],
    properties: {
      type: { type: 'string', pattern: typePattern },

      ...nextStepData,
      ...staticProps,
      ...properties,

      tangles: {
        type: 'object',
        required: [tangle],
        properties: {
          [tangle]: { $ref: '#/definitions/tangle/root' }
        }
      },
      recps: { $ref: '#/definitions/recipients' }
    },
    additionalProperties: false,
    definitions
    // TODO
    // definitions: {
    // }
  }

  return Validator(schema, hooks.isRoot)
}

function buildIsUpdate ({ spec, strategy }) {
  const { typePattern, tangle, nextStepData, hooks } = spec
  const { properties } = strategy.schema

  const schema = {
    type: 'object',
    required: ['type', 'tangles'],
    properties: {
      type: { type: 'string', pattern: typePattern },

      ...nextStepData,
      ...properties,

      tangles: {
        type: 'object',
        properties: {
          [tangle]: { $ref: '#/definitions/tangle/update' }
        },
        required: [tangle]
      },
      recps: { $ref: '#/definitions/recipients' }
    },
    additionalProperties: false,
    definitions
    // TODO
    // definitions: {
    // }
    //
  }

  return Validator(schema, hooks.isUpdate)
}

module.exports = {
  root: buildIsRoot,
  update: buildIsUpdate
}
