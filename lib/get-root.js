const pull = require('pull-stream')
const { getCanonicalContent } = require('./util')

module.exports = function GetRoot (ssb, crut) {
  const { spec } = crut

  const isRoot = msg => crut.isRoot(getCanonicalContent(msg, spec.getTransformation))
  // NOTE: we only care if the messages we're ingesting pass validators *after*
  // they have been mapped by getTransformation (raw msg could have be some archaic format)

  return function getRoot (id, cb) {
    ssb.get({ id, private: true, meta: true }, (err, root) => {
      if (err) return cb(err)

      handleArbitraryRoot(root, (err, root) => {
        if (err) return cb(err)

        if (!isRoot(root)) {
          return cb(new Error(`not a valid ${spec.type}, ${crut.isRoot.errorsString}`))
        }

        // HACK - ensures there's an authors field on the root message
        // this is to work around ssb-crut-authors which assumes all root messages must set
        // authors. Old ssb-profile records did not do this so resolving them would break
        // TODO - remove this hack (e.g. move to opts.getTransformation in ssb-profile)
        if (isEmpty(root.value.content.authors)) {
          root.value.content.authors = {
            [root.value.author]: { [root.value.sequence]: 1 }
          }
        }

        cb(null, root)
      })
    })
  }

  function handleArbitraryRoot (rootMsg, cb) {
    if (!spec.arbitraryRoot) return cb(null, rootMsg)

    // build a mock root message with needed details
    const mock = {
      key: rootMsg.key,
      value: {
        author: rootMsg.value.author,
        content: {
          type: spec.type,
          tangles: {
            [spec.tangle]: { root: null, previous: null }
          }
          // recps decided below
        }
      }
    }

    const { type, recps } = rootMsg.value.content
    if (recps) {
      mock.value.content.recps = recps
      cb(null, mock)
    } // eslint-disable-line
    // NOTE group/init messages
    // - are manually encrypted, and do not have content.recps field
    // - historically could not be decrypted by the author! (ammended now)
    else if (type === 'group/init' || isLegacyGroupInit(rootMsg.value.content)) {
      ssb.tribes.list({ subtribes: true }, (err, groupIds) => {
        if (err) return cb(err)

        pull(
          pull.values(groupIds),
          // TODO modify ssb.tribes.get to include groupId in returned result by default:
          // pull.asyncMap(ssb.tribes.get),
          pull.asyncMap((groupId, cb) => {
            ssb.tribes.get(groupId, (err, info) => {
              if (err) cb(err)
              else cb(null, { groupId, ...info })
            })
          }),
          pull.filter(groupInfo => groupInfo.root === rootMsg.key),
          pull.take(1),
          pull.collect((err, groupInfos) => {
            if (err) return cb(err)

            mock.value.content.recps = [groupInfos[0].groupId]
            mock.value.content.tangles.group = { root: null, previous: null }
            cb(null, mock)
          })
        )
      })
    } // eslint-disable-line
    else cb(null, mock)
  }
}

function isEmpty (obj) {
  if (obj == null) return true
  if (!Object.keys(obj).length) return true

  return false
}

function isLegacyGroupInit (content) {
  return (
    typeof content === 'string' &&
    content.endsWith('.box2') &&
    content.length <= 209 // max historical size
  )
}
