const merge = require('lodash.merge')
const Graph = require('@tangle/graph')

function isObject (obj) {
  if (obj == null) return false
  return typeof obj === 'object'
}

function pick (source, allowed, fallback = null) {
  return allowed.reduce((acc, field) => {
    acc[field] = (field in source) ? source[field] : fallback

    return acc
  }, {})
}

function defaultGetTransformation (m, distance) {
  if (!m.value) console.error(m)
  return m.value.content
}

// CRUT expects to handle messages with content of format:
// {
//   type,
//   ...staticProps, (for root msg)
//   ...nextStepData,
//   ...props,       (includes auto-included field: tombstone)
//   tangles,
//   recps
// }
//
// So this uses getTransformation to generate canonical shaped content for validations
// i.e. isRoot, isUpdate

function getCanonicalContent (msg, getTransformation) {
  if (getTransformation === defaultGetTransformation) return getTransformation(msg)
  // save making more objects unless we need to!

  const content = {
    type: msg.value.content.type,

    ...getTransformation(msg, 0), // distance = 0 is made up...

    tangles: msg.value.content.tangles
    // recps ?
  }
  if (msg.value.content.recps) content.recps = msg.value.content.recps

  return content
}

function MsgToNode (spec) {
  return function msgToNode (msg, d) {
    return {
      key: msg.key,
      previous: msg.value.content.tangles[spec.tangle].previous,
      data: spec.getTransformation(msg, d),
      // Note: getTransformation may include props that are not in the strategy

      author: msg.value.author,
      sequence: msg.value.sequence
    }
  }
}

function MsgsToNodes (spec) {
  const msgToNode = MsgToNode(spec)

  return function msgsToNodes (msgs) {
    if (spec.getTransformation === defaultGetTransformation) {
      return msgs.map(msgToNode)
    }

    const graph = new Graph(
      msgs.map(m => ({
        key: m.key,
        previous: m.value.content.tangles[spec.tangle].previous
      }))
    )

    return msgs
      .sort((a, b) => {
        // if a is in the history of b, it came before b
        if (graph.getHistory(b.key).includes(a.key)) return -1
        // if b is in the history of a, it came before a
        if (graph.getHistory(a.key).includes(b.key)) return +1

        // otherwise, the messages were concurrent, so we break by timestamp
        return a.value.timestamp - b.value.timestamp
      })
      .map((msg, i) => msgToNode(msg, i))
  }
}

module.exports = {
  isObject,
  pick,
  merge,
  defaultGetTransformation,
  getCanonicalContent,
  MsgToNode,
  MsgsToNodes
}
