const { isObject } = require('./util')

module.exports = function IsValidInput (label, allowed) {
  // label *String*
  // allowed *Object*
  const isValidInput = function isValidInput (input) {
    isValidInput.error = null

    if (!isObject(input)) {
      isValidInput.error = new TypeError(`${label} must be an Object, got ${input}`)
      return false
    }

    for (const prop in input) {
      if (!(prop in allowed)) {
        isValidInput.error = new Error(`unallowed ${label}: ${prop}`)
        return false
      }
    }
    return true
  }

  return isValidInput
}
