const test = require('tape')
const { promisify: p } = require('util')

const { SSB, Spec, replicate } = require('./helpers')
const CRUT = require('../')

function publish (ssb, content, cb) {
  ssb.db ? ssb.db.create({ content }, cb) : ssb.publish(content, cb)
}

function delay (fn, timeout = 100) {
  return (...args) => setTimeout(fn, timeout, ...args)
}

test('read', t => {
  const ssb = SSB()
  const spec = Spec()
  const crut = new CRUT(ssb, spec)

  const Māui = ssb.id
  crut.create(
    {
      parent: 'Taranga',
      preferredName: 'Māui',
      attendees: {
        add: [{ id: Māui, seq: 33 }]
      }
    },
    (err, profileId) => {
      t.error(err, 'creates a profile')

      const manualUpdate = {
        type: spec.type,
        attendees: {
          '@Hine-nui-te-pō': { 5000: 1 },
          [Māui]: { 41: -1 }
        },
        tangles: {
          [spec.tangle]: { root: profileId, previous: [profileId] }
        }
      }

      publish(ssb, manualUpdate, (err, update) => {
        t.error(err, 'manually publishes an update')

        crut.read(profileId, (err, profile) => {
          if (err) throw err

          const expected = {
            key: profileId,
            type: spec.type,
            originalAuthor: ssb.id,
            parent: 'Taranga',
            child: null,
            recps: null,
            preferredName: 'Māui',
            legalName: null,
            attendees: {
              [Māui]: [{ start: 33, end: 41 }],
              '@Hine-nui-te-pō': [{ start: 5000, end: null }]
            },
            tombstone: null,
            states: [],
            conflictFields: []
          }

          t.deepEqual(
            profile,
            expected,
            'reads current state'
          )

          ssb.close()
          t.end()
        })
      })
    }
  )
})

test('read - promise', async t => {
  const ssb = SSB()
  const spec = Spec()
  const crut = new CRUT(ssb, spec)

  const Māui = ssb.id
  try {
    const profileId = await crut.create({
      parent: 'Taranga',
      preferredName: 'Māui',
      attendees: {
        add: [{ id: Māui, seq: 33 }]
      }
    })

    const manualUpdate = {
      type: spec.type,
      attendees: {
        '@Hine-nui-te-pō': { 5000: 1 },
        [Māui]: { 41: -1 }
      },
      tangles: {
        [spec.tangle]: { root: profileId, previous: [profileId] }
      }
    }

    publish(ssb, manualUpdate, async (err, update) => {
      t.error(err, 'manually publishes an update')

      const profile = await crut.read(profileId)

      t.deepEqual(
        profile,
        {
          key: profileId,
          type: spec.type,
          originalAuthor: ssb.id,
          parent: 'Taranga',
          child: null,
          recps: null,
          preferredName: 'Māui',
          legalName: null,
          attendees: {
            [Māui]: [{ start: 33, end: 41 }],
            '@Hine-nui-te-pō': [{ start: 5000, end: null }]
          },
          tombstone: null,
          states: [],
          conflictFields: []
        },
        'reads current state'
      )

      ssb.close()
      t.end()
    })
  } catch (err) {
    if (err) throw err
  }
})

test('read (multiple states - conflicting)', { objectPrintDepth: 10 }, async t => {
  const ssb = SSB()
  const spec = Spec()
  const crut = new CRUT(ssb, spec)

  const Māui = ssb.id
  crut.create(
    {
      parent: 'Taranga',
      preferredName: 'Māui',
      attendees: {
        add: [{ id: Māui, seq: 33 }]
      },
      tombstone: null
    },
    (err, profileId) => {
      t.error(err, 'creates a profile')

      const manualUpdateA = {
        type: spec.type,
        preferredName: { set: 'Māui A' },
        tangles: {
          [spec.tangle]: { root: profileId, previous: [profileId] }
        }
      }
      const manualUpdateB = {
        type: spec.type,
        preferredName: { set: 'Māui B' },
        tangles: {
          [spec.tangle]: { root: profileId, previous: [profileId] }
        }
      }
      // NOTE these both branch off from the root (see tangle previous)

      publish(ssb, manualUpdateA, (err, updateA) => {
        t.error(err, 'manually publishes an updateA')

        delay(publish, 100)(ssb, manualUpdateB, (err, updateB) => {
          t.error(err, 'manually publishes an updateB')
          crut.read(profileId, (err, profile) => {
            if (err) throw err

            const states = [
              {
                key: updateB.key,
                preferredName: 'Māui B',
                legalName: null,
                attendees: {
                  [Māui]: [{ start: 33, end: null }]
                },
                tombstone: null
              },
              {
                key: updateA.key,
                preferredName: 'Māui A',
                legalName: null,
                attendees: {
                  [Māui]: [{ start: 33, end: null }]
                },
                tombstone: null
              }
            ]

            t.deepEqual(
              profile,
              {
                key: profileId,
                type: spec.type,
                originalAuthor: ssb.id,
                parent: 'Taranga',
                child: null,
                recps: null,
                preferredName: 'Māui B', // <
                legalName: null,
                attendees: {
                  [Māui]: [{ start: 33, end: null }]
                },
                tombstone: null,
                states,
                conflictFields: ['preferredName']
              },
              'reads current states (and orders by timestamp - latest first, cannot automatic merge)'
            )

            ssb.close()
            t.end()
          })
        })
      })
    }
  )
})

test('read (multiple states - auto-mergeable)', { objectPrintDepth: 10 }, async t => {
  const ssb = SSB()
  const spec = Spec()
  const crut = new CRUT(ssb, spec)

  const profileId = await crut.create({
    parent: 'Taranga',
    preferredName: 'Māui',
    attendees: {
      add: [{ id: ssb.id, seq: 1 }]
    }
  })

  const publishP = p((content, cb) => publish(ssb, content, cb))
  // NOTE these both branch off from the root (see tangle previous)
  await publishP({
    type: spec.type,
    preferredName: { set: 'Māui A' },
    tangles: {
      [spec.tangle]: { root: profileId, previous: [profileId] }
    }
  })
  await new Promise(resolve => setTimeout(resolve, 10)) // ensure not published at same time!
  await publishP({
    type: spec.type,
    legalName: { set: 'Māui B' },
    tangles: {
      [spec.tangle]: { root: profileId, previous: [profileId] }
    }
  })
  // Now we see what read returns
  const profile = await crut.read(profileId)

  const expected = {
    key: profileId,
    type: spec.type,
    originalAuthor: ssb.id,
    parent: 'Taranga',
    child: null,
    recps: null,
    preferredName: 'Māui A',
    legalName: 'Māui B',
    attendees: { [ssb.id]: [{ start: 1, end: null }] },
    tombstone: null,
    states: [],
    conflictFields: []
  }
  t.deepEqual(
    profile,
    expected,
    'Read automatically merges tips into state prop (if all tips are nonconflicting)'
  )

  ssb.close()
  t.end()
})

test('read (isValidNextStep ignores a friends update)', t => {
  const me = SSB()
  const spec = Spec()
  const crut = new CRUT(me, spec)

  const friend = SSB()

  const Māui = me.id
  const Hine = friend.id

  crut.create(
    {
      parent: 'Taranga',
      preferredName: 'Māui',
      attendees: {
        add: [{ id: Māui, seq: 33 }]
      }
    },
    (err, profileId) => {
      t.error(err, 'creates a profile')

      const manualUpdate = {
        type: spec.type,
        attendees: {
          [Māui]: { 41: -1 },
          [Hine]: { 5000: 1 }
        },
        tangles: {
          [spec.tangle]: { root: profileId, previous: [profileId] }
        }
      }

      // Hine tries to publish an update which removes Māui and adds herself!
      publish(friend, manualUpdate, (err, update) => {
        t.error(err, 'friend manually publishes an update')

        replicate({ from: friend, to: me }, (err) => {
          t.error(err, 'I get a copy of this invalid update')

          crut.read(profileId, (err, profile) => {
            if (err) throw err

            const expected = {
              key: profileId,
              type: spec.type,
              originalAuthor: me.id,
              parent: 'Taranga',
              child: null,
              recps: null,
              preferredName: 'Māui',
              legalName: null,
              attendees: {
                [Māui]: [{ start: 33, end: null }]
              },
              tombstone: null,
              states: [],
              conflictFields: []
            }
            t.deepEqual(profile, expected, 'current state ignores invalid update')

            me.close()
            friend.close()
            t.end()
          })
        })
      })
    }
  )
})

test('read (isValidNextStep includes a friends update)', t => {
  const me = SSB()
  const spec = Spec()
  const crut = new CRUT(me, spec)

  const friend = SSB()

  const Māui = me.id
  const Hine = friend.id

  crut.create(
    {
      parent: 'Taranga',
      preferredName: 'Māui',
      attendees: {
        add: [
          { id: Māui, seq: 33 },
          { id: Hine, seq: 5000 }
        ]
      }
    },
    (err, profileId) => {
      t.error(err, 'creates a profile')

      const manualUpdate = {
        type: spec.type,
        attendees: {
          [Māui]: { 41: -1 },
          [Hine]: { 5000: 1 }
        },
        tangles: {
          [spec.tangle]: { root: profileId, previous: [profileId] }
        }
      }

      // Hine publishes an update which removes Māui and adds herself!
      // NOTE for this particular spec removal is not possible
      publish(friend, manualUpdate, (err, update) => {
        t.error(err, 'friend manually publishes an update')

        replicate({ from: friend, to: me }, (err) => {
          t.error(err, 'I get a copy of this update')

          crut.read(profileId, (err, profile) => {
            if (err) throw err

            const expected = {
              key: profileId,
              type: spec.type,
              originalAuthor: me.id,
              parent: 'Taranga',
              child: null,
              recps: null,
              preferredName: 'Māui',
              legalName: null,
              attendees: {
                [Māui]: [{ start: 33, end: 41 }],
                [Hine]: [{ start: 5000, end: null }]
              },
              tombstone: null,
              states: [],
              conflictFields: []
            }
            t.deepEqual(profile, expected, 'current state ignores invalid update')

            me.close()
            friend.close()
            t.end()
          })
        })
      })
    }
  )
})

test('read (recps)', t => {
  const ssb = SSB({ tribes: true })
  const spec = Spec()
  const crut = new CRUT(ssb, spec)

  const Māui = ssb.id
  crut.create(
    {
      parent: 'Taranga',
      preferredName: 'Māui',
      attendees: {
        add: [{ id: Māui, seq: 33 }]
      },
      recps: [ssb.id]
    },
    (err, profileId) => {
      t.error(err, 'creates a profile')

      const manualUpdate = {
        type: spec.type,
        attendees: {
          '@Hine-nui-te-pō': { 5000: 1 },
          [Māui]: { 41: -1 }
        },
        tangles: {
          [spec.tangle]: { root: profileId, previous: [profileId] }
        }
      }

      publish(ssb, manualUpdate, (err, update) => {
        t.error(err, 'manually publishes an update')

        crut.read(profileId, (err, profile) => {
          if (err) throw err

          t.deepEqual(
            profile,
            {
              key: profileId,
              type: spec.type,
              originalAuthor: ssb.id,
              parent: 'Taranga',
              child: null,
              recps: [ssb.id],
              preferredName: 'Māui',
              legalName: null,
              attendees: {
                [Māui]: [{ start: 33, end: 41 }],
                '@Hine-nui-te-pō': [{ start: 5000, end: null }]
              },
              tombstone: null,
              states: [],
              conflictFields: []
            },
            'reads current state'
          )

          ssb.close()
          t.end()
        })
      })
    }
  )
})

test('read (getTransformation)', t => {
  const ssb = SSB()
  const spec = Spec({
    getTransformation: m => {
      const { author, content } = m.value

      const _content = { ...content }
      if (_content.preferredName) {
        _content.preferredName = {
          set: { author, value: _content.preferredName.set }
        }
      }
      return _content
    }
  })
  const crut = new CRUT(ssb, spec)

  const Māui = ssb.id
  crut.create(
    {
      parent: 'Taranga',
      preferredName: 'Māui',
      legalName: 'Ben',
      attendees: {
        add: [{ id: Māui, seq: 33 }]
      }
    },
    (err, profileId) => {
      t.error(err, 'creates a profile')

      crut.read(profileId, (err, profile) => {
        if (err) throw err

        t.deepEqual(
          profile,
          {
            key: profileId,
            type: spec.type,
            originalAuthor: ssb.id,
            parent: 'Taranga',
            child: null,
            recps: null,
            preferredName: { author: Māui, value: 'Māui' },
            legalName: 'Ben',
            attendees: {
              [Māui]: [{ start: 33, end: null }]
            },
            tombstone: null,
            states: [],
            conflictFields: []
          },
          'reads decorated info'
        )

        ssb.get({ id: profileId, meta: true, private: true }, (err, m) => {
          if (err) throw err
          t.equal(m.value.content.preferredName.set, 'Māui', 'saves simple info')

          ssb.close()
          t.end()
        })
      })
    }
  )
})

test('create + read (typePattern)', t => {
  const ssb = SSB()
  const spec = Spec({
    type: 'profile/*',
    typePattern: '^profile\\/\\*$',
    staticProps: {}
  })
  const crut = new CRUT(ssb, spec)

  crut.create(
    {},
    (err, profileId) => {
      t.error(err, 'use #create')

      ssb.get(profileId, (err, value) => {
        if (err) throw err

        t.equals(value.content.type, 'profile/*', 'creates correct root message')

        const content = {
          type: 'profile/*',
          tangles: {
            profile: { root: null, previous: null }
          }
        }
        publish(ssb, content, (err, m) => {
          if (err) throw err

          crut.read(m.key, (err, data) => {
            t.error(err, 'uses typePattern correctly')

            const badContent = {
              type: 'profile/w',
              tangles: {
                profile: { root: null, previous: null }
              }
            }
            publish(ssb, badContent, (err, m) => {
              if (err) throw err

              crut.read(m.key, (err, data) => {
                t.match(err.message, /not a valid profile\/\*/)

                ssb.close()
                t.end()
              })
            })
          })
        })
      })
    }
  )
})

test('create, update + read (nextStepData)', t => {
  const ssb = SSB()
  const spec = Spec({
    type: 'profile',
    staticProps: {},
    nextStepData: {
      extraInfo: { type: 'string' }
    }
  })
  const crut = new CRUT(ssb, spec)

  crut.create({
    extraInfo: 'create info',
    attendees: {
      add: [{ id: ssb.id, seq: 33 }]
    }
  },
  (err, profileId) => {
    t.error(err, 'use #create')

    crut.update(profileId, { extraInfo: 'update info' },
      (err) => {
        t.error(err, 'use #update')

        crut.read(profileId, (err, data) => {
          if (err) throw err

          t.equal(data.conflictFields.length, 0, 'no conflicts')
          t.equal(data.extraInfo, undefined, 'nextStepData not in states')

          // manual publish
          const content = {
            type: 'profile',
            extraInfo: 'new update info',
            tangles: {
              profile: { root: null, previous: null }
            }
          }
          publish(ssb, content, (err, m) => {
            if (err) throw err

            crut.read(m.key, (err, data) => {
              if (err) throw err

              t.equal(data.conflictFields.length, 0, 'no conflicts')
              t.equal(data.extraInfo, undefined, 'nextStepData not in states')

              ssb.close()
              t.end()
            })
          })
        })
      })
  }
  )
})
