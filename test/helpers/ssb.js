const Stack = require('scuttle-testbot')

module.exports = function (opts = {}) {
  // opts = {
  //   name: String,
  //   startUnclean: Boolean,
  //   keys: SecretKeys
  //
  //   recpsGuard: Boolean,
  //   tribes: Boolean
  // }

  return process.env.DB2
    ? db2SSB(opts)
    : db1SSB(opts)
}

function db1SSB (opts) {
  let stack = Stack // eslint-disable-line
    .use(require('ssb-backlinks'))
    .use(require('ssb-query'))

  // only add ssb-tribes when testing recps, as keystore startup takes 500ms
  if (opts.tribes || opts.recpsGuard) {
    stack = stack
      .use(require('ssb-tribes-db1'))
  }

  if (opts.recpsGuard === true) {
    stack = stack.use(require('ssb-recps-guard'))
  }

  return stack({
    db1: true,
    ...opts
  })
}

function db2SSB (opts) {
  let stack = Stack // eslint-disable-line
    .use(require('ssb-db2/core'))
    .use(require('ssb-classic'))
    .use(require('ssb-db2/compat/db'))
    .use(require('ssb-db2/compat/history-stream'))
    .use(require('ssb-db2/compat/feedstate'))
    .use(require('ssb-db2/compat/post'))
    .use(require('ssb-box2'))

  if (opts.tribes || opts.recpsGuard) {
    stack = stack
      .use(require('ssb-tribes'))
  }

  if (opts.recpsGuard === true) {
    stack = stack.use(require('ssb-recps-guard'))
  }

  return stack({
    noDefaultUse: true,
    ...opts,
    box2: {
      legacyMode: true,
      ...opts.box2
    }
  })
}
