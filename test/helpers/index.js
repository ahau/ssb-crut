const { replicate } = require('scuttle-testbot')

module.exports = {
  SSB: require('./ssb'),
  Spec: require('./spec.mock.js'),
  replicate,
  fixRecps,
  setupForked: require('./setup-forked')
}

function fixRecps (obj, ssb) {
  // this is a db2 compat thing
  return Object.assign(
    obj,
    ssb.db ? { recps: undefined } : undefined
  ) // db2 leaves these
}
