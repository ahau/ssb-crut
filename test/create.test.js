const { promisify: p } = require('util')
const test = require('tape')
const pull = require('pull-stream')
const { isMsg } = require('ssb-ref')
const keys = require('ssb-keys')

const { SSB, Spec, fixRecps } = require('./helpers')
const CRUT = require('../')

test('create (minimal)', t => {
  const ssb = SSB()
  const spec = Spec()
  const crut = new CRUT(ssb, spec)

  crut.create(
    { parent: 'mix' },
    (err, profileId) => {
      t.error(err, 'use #create')
      t.true(isMsg(profileId), 'calls back with MsgId')

      ssb.get(profileId, (err, value) => {
        if (err) throw err

        t.deepEquals(
          value.content,
          fixRecps({
            type: spec.type,
            parent: 'mix',
            tangles: {
              [spec.tangle]: { root: null, previous: null }
            }
          }, ssb),
          'publishes correct message'
        )
        ssb.close()
        t.end()
      })
    }
  )
})

test('create (minimal) - promise', async t => {
  const ssb = SSB()
  const spec = Spec()
  const crut = new CRUT(ssb, spec)
  try {
    const profileId = await crut.create({ parent: 'mix' })
    t.true(isMsg(profileId), 'calls back with MsgId')

    ssb.get(profileId, (err, value) => {
      if (err) throw err

      t.deepEquals(
        value.content,
        fixRecps({
          type: spec.type,
          parent: 'mix',
          tangles: {
            [spec.tangle]: { root: null, previous: null }
          }
        }, ssb),
        'publishes correct message'
      )
      ssb.close()
      t.end()
    })
  } catch (err) {
    t.error(err, 'use #create')
  }
})

test('create (props)', t => {
  const ssb = SSB()
  const spec = Spec()
  const crut = new CRUT(ssb, spec)

  crut.create(
    {
      parent: 'mix',
      child: 'ira',
      preferredName: 'Maui',
      attendees: { add: [{ id: ssb.id, seq: 22 }] }
    },
    (err, profileId) => {
      t.error(err, 'use #create')
      t.true(isMsg(profileId), 'calls back with MsgId')

      ssb.get(profileId, (err, value) => {
        if (err) throw err

        t.deepEquals(
          value.content,
          fixRecps({
            type: 'profile/person', // from spec
            parent: 'mix',
            child: 'ira',
            preferredName: { set: 'Maui' },
            attendees: {
              [ssb.id]: { 22: 1 }
            },
            tangles: {
              profile: { root: null, previous: null }
            }
          }, ssb),
          'publishes correct message'
        )
        ssb.close()
        t.end()
      })
    }
  )
})

test('create (malformed attrs)', t => {
  const ssb = SSB()
  const spec = Spec()
  const crut = new CRUT(ssb, spec)

  const tests = [
    {
      input: 'dogo',
      expectedErr: 'input must be an Object'
    },
    // {
    //   input: ['dogo'],
    //   expectedErr: 'input must be an Object'
    // },
    {
      input: { dogo: 'pup' },
      expectedErr: 'unallowed inputs: dogo'
    },
    {
      input: null,
      expectedErr: 'input must be an Object'
    },
    {
      input: undefined,
      expectedErr: 'input must be an Object'
    },
    {
      input: 0,
      expectedErr: 'input must be an Object'
    }
  ]

  pull(
    pull.values(tests),
    pull.asyncMap(({ input, expectedErr }, cb) => {
      crut.create(input, (err) => {
        if (!err) {
          return cb(new Error(`Expected to see error '${expectedErr}'`))
        }

        t.match(err.message, new RegExp(expectedErr, 'g'), expectedErr)
        cb(null, null)
      })
    }),
    pull.collect((err) => {
      if (err) throw err

      ssb.close()
      t.end()
    })
  )
})

test('create (ssb-recps-guard)', t => {
  let ssb = SSB({ recpsGuard: true })
  const spec = Spec()
  let crut = new CRUT(ssb, spec)

  crut.create({ parent: 'mix' }, (err) => {
    t.match(
      err.message,
      /(recps-guard: public messages .* not allowed|tribes.publish requires content.recps)$/,
      'recps guard blocks public message'
    )

    crut.create({ parent: 'mix', recps: [ssb.id] }, (err) => {
      t.error(err, 'allows a message with recps to be published')

      crut.create({ parent: 'mix', allowPublic: true }, (err) => {
        t.error(err, 'allows a public message with recps: "allowPublic"')

        ssb.close()
        ssb = SSB({
          recpsGuard: {
            allowedTypes: ['profile/person']
          }
        })
        crut = new CRUT(ssb, spec)

        crut.create({ parent: 'mix' }, (err) => {
          t.error(err, 'can create is type is in recpsGuard allowedTypes')

          ssb.close()
          t.end()
        })
      })
    })
  })
})

test('create (getTransformation)', t => {
  const ssb = SSB()

  const spec = Spec({
    getTransformation: m => {
      const { author, content } = m.value

      const _content = { ...content } // shallow clone so we're not mutating original content
      if (_content.preferredName) {
        _content.preferredName = {
          set: { author, value: _content.preferredName.set }
        }
      }

      return _content
    }
  })
  const crut = new CRUT(ssb, spec)

  const Māui = ssb.id
  crut.create(
    {
      parent: 'Taranga',
      preferredName: 'Māui',
      legalName: 'Ben',
      attendees: {
        add: [
          { id: Māui, seq: 33 }
        ]
      }
    },
    (err, profileId) => {
      t.error(err, 'creates a profile')

      ssb.get(profileId, (err, value) => {
        if (err) throw err

        t.deepEqual(value.content.preferredName, { set: 'Māui' }, 'persists in undecorated state')

        ssb.close()
        t.end()
      })
    }
  )
})

test('create, opts = { feedId, create }', t => {
  const otherKeys = keys.generate()

  const ssb = SSB({ recpsGuard: true })
  const spec = Spec()
  const crut = new CRUT(ssb, spec, {
    feedId: otherKeys.id,
    create (input, cb) {
      process.env.DB2
        ? ssb.db.create({
          keys: otherKeys,
          content: input.content,
          encryptionFormat: 'box2'
        }, cb)
        : ssb.publish(input.content, cb)
    }
  })

  crut.create({ parent: 'mix', recps: [ssb.id] }, (err, msgKey) => {
    t.error(err, 'creates')
    ssb.get(msgKey, (err, value) => {
      if (err) t.error(err, 'reads msg with id ' + msgKey)
      if (process.env.DB2) t.equal(value.author, otherKeys.id, 'create with proper author')

      ssb.close()
      t.end()
    })
  })
})

test('create with recps but without tribes in db2', async t => {
  if (!process.env.DB2) {
    console.log('DB2 only test')
    t.end()
    return
  }
  const ssb = SSB()
  const spec = Spec()
  const crut = new CRUT(ssb, spec)

  const profileId = await crut.create({
    parent: 'Taranga',
    preferredName: 'Māui',
    attendees: {
      add: [{ id: ssb.id, seq: 33 }]
    },
    recps: [ssb.id]
  })
    .catch(t.error)

  t.true(profileId, 'creates a profile')

  await p(ssb.close)()
  t.end()
})
