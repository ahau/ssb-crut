# Tests

Note the order these tests were built in:
1. create
2. read
  - uses `crut.create` (1) to create root message
  - **manually** published an update
  - uses `crut.read` to check these messages are reduced right
3. update
  - uses `crut.create` (1) to create root message
  - uses `crut.update` to  publish an update
  - uses `crut.red` (2) to check update was published correctly
4. tombstone
  - special case of (3)
  - only relevant if props has a `tombstone` field
  - TODO
5. merge
  - spans read and update scenarios for merging
