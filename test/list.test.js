const test = require('tape')
const Overwrite = require('@tangle/overwrite')
const { promisify } = require('util')

const { SSB, replicate } = require('./helpers')
const CRUT = require('../')

const spec = {
  type: 'profile/test',
  tangle: 'profile',
  props: {
    name: Overwrite()
  }
}

test('list', async t => {
  /* setup */
  // make a friend who has made a record first
  const friend = SSB()
  await new CRUT(friend, spec).create({ name: 0 })

  // meanwhile I publish a bunch of records
  const me = SSB({ tribes: true })
  const crut = new CRUT(me, spec)
  const create = async (name, recps) => {
    const id = await crut.create({ name, recps })
    if (process.env.DB2) {
      await promisify(setTimeout)(50)
      // HACK db2 gets orderBy=createTime wrong sometimes without this
    }
    return id
  }

  await create(1)
  const secondId = await create(2)
  await create(3)
  await crut.update(secondId, { name: 2.0 })
  // order of creation: 3, 2, 1, 0 (descending)
  // order of update: 2, 3, 1, 0 (descending)

  /* opts = {} */
  let list = await crut.list()
  t.deepEqual(
    list.map(record => record.name),
    [3, 2.0, 1],
    'no opts'
  )

  /* opts.limit */
  list = await crut.list({ limit: 2 })
  t.deepEqual(
    list.map(record => record.name),
    [3, 2.0],
    'opts.limit'
  )

  /* opts.descending */
  // limit fail
  try {
    list = await crut.list({ limit: '2' })
  } catch (ex) {
    t.equal(ex.message, 'opts.limit must be a number', 'opts.limit string')
  }

  // ascending
  list = await crut.list({ descending: false })
  t.deepEqual(
    list.map(record => record.name),
    [1, 2.0, 3],
    'opts.descending = false (ascending)'
  )

  /* opts.startFrom */
  list = await crut.list({ startFrom: secondId })
  t.deepEqual(
    list.map(record => record.name),
    [1],
    'opts.startFrom'
  )

  /* opts.orderBy */
  await replicate({ from: friend, to: me })

  list = await crut.list() // orderBy: receiveTime
  t.deepEqual(
    list.map(record => record.name),
    [0, 3, 2.0, 1],
    'opts.orderBy = receiveTime (after replicate))'
  )

  list = await crut.list({ orderBy: 'updateTime' })
  t.deepEqual(
    list.map(record => record.name),
    [0, 2.0, 3, 1],
    'opts.orderBy = updateTime (after replicate))'
  )

  list = await crut.list({ orderBy: 'createTime' })
  t.deepEqual(
    list.map(record => record.name),
    [3, 2.0, 1, 0],
    'opts.orderBy = createTime (after replicate))'
  )

  /* opts.filter */
  list = await crut.list({
    filter: record => {
      return Math.floor(record.name) % 2 === 0
    }
  }).catch(t.error)
  t.deepEqual(
    list.map(record => record.name),
    [0, 2.0], // NOTE 'receivedTime' is the default orderBy
    'opts.filter'
  )

  /* opts.read */
  list = await crut.list({
    read: (id, cb) => {
      crut.read(id, (err, record) => {
        if (err) return cb(null, null)

        record.fancyName = 'fancy-' + record.name
        delete record.states
        cb(null, record)
      })
    }
    // we provide a custom read which adjusts
    // but could also check a cache etc
  }).catch(t.error)

  t.deepEqual(
    list.map(record => record.fancyName),
    ['fancy-0', 'fancy-3', 'fancy-2', 'fancy-1'],
    'opts.read'
  )

  // tombstone
  await crut.tombstone(secondId, {})
  list = await crut.list()
  t.deepEqual(
    list.map(record => record.name),
    [0, 3, 1], // NOTE 'receivedTime' is the default orderBy
    'tombstone ignored'
  )
  list = await crut.list({ tombstoned: true })
  t.deepEqual(
    list.map(record => record.name),
    [2.0],
    'opts.tombstoned = true'
  )

  /* opts.groupId */
  const { groupId } = await promisify(me.tribes.create)({})
  await create(4, [groupId])
  list = await crut.list({ groupId })
  t.deepEqual(
    list.map(record => record.name),
    [4],
    'opts.groupId'
  )

  me.close()
  friend.close()
  t.end()
})
