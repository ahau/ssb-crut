/* eslint-disable brace-style */
const Strategy = require('@tangle/strategy')
const Overwrite = require('@tangle/overwrite')
const { tombstone } = require('ssb-schema-definitions')()
const { promisify } = require('util')
const pull = require('pull-stream')
const paraMap = require('pull-paramap')

const isValidSpec = require('./lib/is-valid-spec')
const IsValidMsg = require('./lib/is-valid-msg')
const IsValidInput = require('./lib/is-valid-input')
const ResolveTangle = require('./lib/resolve-tangle')
const HandlePublish = require('./lib/handle-publish')
const CreateStream = require('./lib/create-stream')
const { merge, isObject, pick, defaultGetTransformation } = require('./lib/util')

const {
  PROTECTED_PROPS,
  PROTECTED_STATIC_PROPS,
  PROTECTED_NEXT_STEP_DATA
} = require('./constants')

const defaultCreate = (ssb) => {
  // if we're db1
  if (!ssb.db) return defaultCreateDB1
  // if we're db2
  else return defaultCreateDB2

  function defaultCreateDB1 (input, cb) {
    if (input.allowPublic) ssb.publish(input, cb)
    // NOTE if we run ssb.publish({ content, allowPublic }, cb)
    // and dont't have ssb-recps-guard installed, we get an error,
    // but that's ok (intended behaviour)
    else ssb.publish(input.content, cb)
  }

  function defaultCreateDB2 (input, cb) {
    const noEncryptionRequired = (
      !input.content.recps || // => intended to be public
      typeof input.content === 'string' // => already encrypted
    )

    if (noEncryptionRequired) ssb.db.create(input, cb)
    else {
      if (ssb.tribes && ssb.tribes.publish) {
        ssb.tribes.publish(input.content, cb)
      }
      else {
        ssb.db.create({
          ...input,
          ...(ssb.box2 ? { encryptionFormat: 'box2' } : {})
        }, cb)
        // WARNING will be encrypted but will lack group tangle data
        // as ssb.tribes.publish takes care of that
      }
    }
  }
}

module.exports = class CRUT {
  constructor (ssb, spec, opts = {}) {
    checkServerDeps(ssb)
    checkReservedValues(spec)
    checkOpts(opts)

    const {
      create = defaultCreate(ssb),
      feedId = ssb.id,
      alwaysIncludeStates = false
    } = opts

    if (opts.publish) {
      throw Error('opts.publish no longer supported, please use opts.create')
    }

    this.spec = merge(
      {
        typePattern: spec.typePattern || `^${spec.type}$`,
        tangle: spec.type && spec.type.split('/')[0],
        props: {
          tombstone: Overwrite({ valueSchema: tombstone })
        },
        staticProps: {},
        nextStepData: {},
        isValidNextStep: () => true,
        hooks: {
          isRoot: (spec && spec.hooks && spec.hooks.isRoot) || [],
          isUpdate: (spec && spec.hooks && spec.hooks.isUpdate) || []
        },
        getTransformation: defaultGetTransformation,
        arbitraryRoot: false
      },
      spec
    )
    if (!isValidSpec(this.spec)) throw isValidSpec.error
    this.strategy = new Strategy(this.spec.props)

    this.isRoot = IsValidMsg.root(this)
    this.isUpdate = IsValidMsg.update(this)
    this.isValidUpdateInput = IsValidInput('update input', {
      ...this.spec.props,
      ...this.spec.nextStepData
    })

    this.resolveTangle = ResolveTangle(ssb, this)
    this.handlePublish = HandlePublish({ crut: this, create, feedId, ssb })

    if (this.spec.arbitraryRoot) this.getTribe = (groupId, cb) => ssb.tribes.get(groupId, cb)
    // NOTE ssb.tribes is not accessible while in ssb-tribes#init, this side-steps that

    this.createStream = CreateStream(ssb, this.spec)
    this.getRecordId = GetRecordId(this.spec)
    this.alwaysIncludeStates = alwaysIncludeStates
  }

  create (input, cb) {
    if (cb === undefined) return promisify(this.create).call(this, input)

    if (this.spec.arbitraryRoot) return cb(new Error('cannot call create on a crut with spec.arbitraryRoot'))
    if (!isObject(input)) return cb(new Error('input must be an Object'))

    const {
      props, staticProps, nextStepData, recps, allowPublic, unknown
    } = sortInput(this.spec, input)
    if (Object.keys(unknown).length) {
      return cb(new Error(`unallowed inputs: ${Object.keys(unknown)}`))
    }

    let propsT
    try {
      propsT = this.strategy.mapFromInput(
        props,
        [this.strategy.identity()]
      )
    } catch (e) { return cb(e) }

    const content = {
      type: this.spec.type,
      ...nextStepData,
      ...staticProps,
      ...propsT,

      recps,
      tangles: {
        [this.spec.tangle]: { root: null, previous: null }
      }
    }

    // we mock a an empty initial transformation state so we can check `isValidNextStep`
    // on the initial step (root message)
    const tangleContext = {
      root: null, // DNE
      tips: [{
        key: null, // DNE
        T: this.strategy.identity()
      }],
      graph: null // DNE
    }

    this.handlePublish(tangleContext, content, allowPublic, cb)
  }

  read (id, cb) {
    if (cb === undefined) return promisify(this.read).call(this, id)

    this.resolveTangle(id, (err, tangle) => {
      if (err) return cb(err)

      const result = {
        key: id,
        type: this.spec.type,
        originalAuthor: tangle.root.value.author,
        recps: tangle.root.value.content.recps || null,

        ...pick(
          tangle.root.value.content,
          Object.keys(this.spec.staticProps)
        ),
        ...getState(this.strategy, tangle, this.alwaysIncludeStates)
      }

      cb(null, result)
    })
  }

  update (id, input, cb) {
    if (cb === undefined) return promisify(this.update).call(this, id, input)

    const allowPublic = input.allowPublic
    delete input.allowPublic
    if (!this.isValidUpdateInput(input)) return cb(this.isValidUpdateInput.error)

    this.resolveTangle(id, (err, tangle) => {
      if (err) return cb(err)

      const { props, nextStepData } = sortInput(this.spec, input)

      const content = {
        type: this.spec.type,
        ...this.strategy.mapFromInput(
          props,
          tangle.tips.map(tip => tip.T) // currentTips state
        ),
        ...nextStepData,
        recps: tangle.root.value.content.recps,
        tangles: {
          [this.spec.tangle]: {
            root: id,
            previous: tangle.tips.map(tip => tip.key) // currentTips keys
          }
        }
      }

      this.handlePublish(tangle, content, allowPublic, cb)
    })
  }

  list (opts, cb) {
    if (opts === undefined) opts = {}
    if (cb === undefined) return promisify(this.list).call(this, opts)
    if (opts.limit && typeof opts.limit !== 'number') { throw new Error('opts.limit must be a number') }

    let hasReachedStartFrom = false

    pull(
      this.createStream(opts),

      opts.startFrom
        ? pull.filter(msg => {
          if (hasReachedStartFrom) return true
          if (msg.key === opts.startFrom) hasReachedStartFrom = true
          return false
        })
        : null,

      opts.groupId
        ? pull.filter(msg => {
          if (!msg.value.content.recps) return false
          return msg.value.content.recps[0] === opts.groupId
        })
        : null,

      pull.map(this.getRecordId),
      pull.unique(),

      // map rootMsg => record (and filter out the ones which were junk)
      paraMap(
        opts.read || ((id, cb) => {
          this.read(id, (err, record) => {
            if (err) cb(null, false)
            else cb(null, record)
          })
        }),
        opts.width || 5
      ),
      pull.filter(Boolean),

      // filter out the tombstoned
      opts.tombstoned
        ? pull.filter(record => record.tombstone !== null)
        : pull.filter(record => record.tombstone === null),

      opts.filter
        ? pull.filter(opts.filter)
        : null,

      // limit
      opts.limit ? pull.take(opts.limit) : null,
      pull.collect(cb)
    )
  }

  tombstone (id, input, cb) {
    if (cb === undefined) return promisify(this.tombstone).call(this, id, input)
    const _input = Object.keys(input).reduce(
      (acc, key) => {
        if (
          key === 'allowPublic' ||
          key in this.spec.nextStepData
        ) acc[key] = input[key]
        return acc
      },
      {
        tombstone: input.undo === true
          ? null
          : { date: Date.now(), reason: input.reason }
      }
    )

    this.update(id, _input, cb)
  }

  /* special group methods */
  updateGroup (groupId, input, cb) {
    if (cb === undefined) return promisify(this.updateGroup).call(this, groupId, input)

    if (!this.spec.arbitraryRoot) return cb(new Error('cannot use updateGroup with spec.arbitraryRoot = false'))

    this.getTribe(groupId, (err, info) => {
      if (err) return cb(err)
      this.update(info.root, input, cb)
    })
  }

  readGroup (groupId, cb) {
    if (cb === undefined) return promisify(this.readGroup).call(this, groupId)

    if (!this.spec.arbitraryRoot) return cb(new Error('cannot use readGroup with spec.arbitraryRoot = false'))

    this.getTribe(groupId, (err, info) => {
      if (err) return cb(err)
      this.read(info.root, cb)
    })
  }

  tombstoneGroup (groupId, input, cb) {
    if (cb === undefined) return promisify(this.tombstoneGroup).call(this, groupId, input)

    if (!this.spec.arbitraryRoot) return cb(new Error('cannot use tombstoneGroup with spec.arbitraryRoot = false'))

    this.getTribe(groupId, (err, info) => {
      if (err) return cb(err)
      this.tombstone(info.root, input, cb)
    })
  }
}

function sortInput (spec, input) {
  return Object.keys(input).reduce(
    (acc, key) => {
      if (key === 'recps') acc.recps = input[key]
      else if (key === 'allowPublic') acc.allowPublic = input[key]
      else if (key in spec.props) acc.props[key] = input[key]
      else if (key in spec.staticProps) acc.staticProps[key] = input[key]
      else if (key in spec.nextStepData) acc.nextStepData[key] = input[key]
      else acc.unknown[key] = input[key]

      return acc
    },
    {
      props: {},
      staticProps: {},
      nextStepData: {},
      recps: undefined,
      allowPublic: false,
      unknown: {}
    }
  )
}

function checkServerDeps (ssb) {
  const db1Support = Boolean(ssb.backlinks) && Boolean(ssb.query)
  const db2Support = Boolean(ssb.db)

  if (!db1Support && !db2Support) {
    throw new Error('ssb-crut requires EITHER (ssb-db + ssb-backlinks + ssb-query) OR ssb-db2')
  }
}

function checkReservedValues (spec) {
  if (spec.props) {
    Object.keys(spec.props).forEach(prop => {
      if (PROTECTED_PROPS.has(prop)) throw new Error(`Invalid spec: spec.props.${prop} is a reserved property`)
    })
  }
  if (spec.staticProps) {
    Object.keys(spec.staticProps).forEach(prop => {
      if (PROTECTED_STATIC_PROPS.has(prop)) throw new Error(`Invalid spec: spec.staticProps.${prop} is a reserved property`)
    })
  }
  if (spec.nextStepData) {
    Object.keys(spec.nextStepData).forEach(prop => {
      if (PROTECTED_NEXT_STEP_DATA.has(prop)) throw new Error(`Invalid spec: spec.nextStepData.${prop} is a reserved property`)
    })
  }
}

function checkOpts (opts) {
  if (opts.create) {
    if (typeof opts.create !== 'function') throw new Error('opts.create must be a function')
    if (typeof opts.feedId !== 'string') throw new Error('opts.feedId must be provided if opts.create is used')
  }
}

function GetRecordId (spec) {
  return function getRecordId (msg) {
    return msg.value.content.tangles[spec.tangle].root === null
      ? msg.key
      : msg.value.content.tangles[spec.tangle].root
  }
}

function getState (strategy, tangle, alwaysIncludeStates) {
  let bestGuessState
  let conflictFields = []
  let states = []

  if (tangle.tips.length === 1) {
    bestGuessState = strategy.mapToOutput(tangle.tips[0].T)
  }
  // However, if there are multiple states we first try to automatically merge them.
  else {
    const dummyNode = {
      // key: '%dummyMessageId',
      previous: tangle.tips.map(({ key, T }) => key),
      data: strategy.identity()
      // author: '%dummyMessageAuthor',
      // sequence: (sequence || 0) + 1
    }
    // If we can automatically merge the tips, then the best guess of state is the merged tips
    if (strategy.isValidMerge(tangle.graph, dummyNode)) {
      const T = strategy.merge(tangle.graph, dummyNode)
      bestGuessState = strategy.mapToOutput(T)
      // these are never needed if there was no conflict
    }
    // Otherwise, we fall back to the tip with the latest entry
    else {
      conflictFields = strategy.isValidMerge.fields
      bestGuessState = strategy.mapToOutput(tangle.tips[0].T)
    }
  }

  if (conflictFields.length || alwaysIncludeStates) {
    states = tangle.tips
      .map(({ key, T }) => ({ key, ...strategy.mapToOutput(T) }))
  }

  return {
    ...bestGuessState,
    conflictFields,
    states
  }
}
