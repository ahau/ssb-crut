# Changelog

## v6.0.0

Breaking:
- `opts.publish` was replaced with `opts.create` as a reflection of the API now closely reflecting `ssb-db2`'s `ssb.db.create` signature.

## v5.0.0

Breaking:
- `crut.list` now supports more options for `opts.orderBy`, but to support this needed to
  change the API of `opts.read` for that method

## v4.0.0

### new features
- merging!
    - new `crut.update` behaviour for branched state:
        - updates auto-merge branched state where possible
        - if there's a conflict you get an error back with `err.conflictFields` which tell you what needs addressing
    - new `crut.read` behaviour:
        - spreads "best guess" props state into root of `record`
        - if there's a branched state, this is the auto-merged state (if possible) OR the most recently updated branch
        - adds `record.conflictFields` - an Array of fields there are conflicts on if the state is branched
- publishAs type API for DB2
    - see `opts.publish` and `opts.feedId` on initialization
- better spec checks:
    - updated protected fields to include `states, key`!
    - protect against use of same fieldName in `props` AND `staticProps`


### breaking changes
- signature of `opts.isValidNextStep` has changed
- all strategies passed into `spec.props` fields now must have an `isValidMerge` function
    - you will get a error if it doesn't
    - all the common strategies have new versions supporting this,just install latest versions


### thanks

Big thanks to @cpilbrow + @arj03 for all the contributions (code and discourse that informed it)

Colin's work on detecting merge conflicts was a major contibution that made this releease possible.


